# Pile Driving Construction Site : You Can Reduce Accidents By 90% #

Pile Operations initiate soon after the conclusion of pre-construction planning.  The commencement of the tasks trigger a hazardous situation and a concerning time for the pile driving contractors. The potential of accident associated with pile driving elevates the significance of maintaining safety gears throughout the job. A contractor needs to focus on workers, support, equipment, site condition, materials and every other factor to conduct the project smoothly.

### Pile Driving Safety Tips- ###

* Concentrate on the FIRST STEP 
The initial stage of pile driving needs more emphasis to ensure a high-quality finishing.  At the onset of a project, the superintendent needs to convey the safety rules to all the employees through a formal meeting. It is even better if printed hard copies are distributed among the workers.
On this meeting, the superintendent must collect the contact numbers of those to be telephoned in emergencies. He even needs to arrange for fulsomely stocked first-aid kits and a trained first aider. A thorough check on nearby hospital and ambulance service is another significant point to focus on.
### Ensure at most security for the workers ###

* BRIMLESS HARD PLASTIC HATS FOR ALL THE WORKERS
* COVER WIELDING GOGGLES WITH 3-4 TINTED LENSES AND EYECUPS FOR THE BURNERS AND CUTTERS
* CHIPPING SAFETY GLASSES WITH CLEAR LENSES FOR THOSE WHO ARE APPOINTED AT CHIPPING AND GRINDING
* WELDING HELMETS FOR WELDERS WITH SHADE 10-12 LENSES. PORTABLE SHIELDS AND SHADE 2 GLASSES WITH SIDE SHIELDS MUST BE PROVIDED IN CASE OF EXTENSIVE WELDING
* NEOPRENE-COATED CANVAS GLOVES FOR WORKERS WHO DEALS WITH CREOSOTED PILES, PLASTIC SLATE AND CEMENT
* LIFE JACKETS AND EMERGENCY BOATS FOR WORKERS HANDLING MARINE OPERATIONS
*A 6 MULTIPURPOSE TYPE 2A-10BC DRY CHEMICAL EXTINGUISHER AT THE ONSITE OFFICE, CHANGE AREA OR TOOL HOUSE
*2 TYPE 5BC DRY CHEMICAL EXTINGUISHERS ON EACH CRANE AND MAJOR EQUIPMENT

### Maintain safety at the site ###

Onsite projects need much more wariness to avoid any sort of menace. The work area as well as the walkways must be kept free of debris, scrap lumbers, pile cut-offs and other loose materials. A secure handrail and a toe board must be setup for walkways and platforms four feet above the ground or located in a perilous location.

The contractors must supply proper containers for trash, oil rags and other combustible materials. Special precautions are required for pile drivers working in potentially combustible environments.

[Ground Anchors](https://piledrivingcontractor.wordpress.com/2018/04/18/piling-construction-safety-tips/)
